import { BrowserRouter as Router } from "react-router-dom";

import Header from "../components/Header";
import Footer from "../components/Footer";

type LayoutProps = {
  children: React.ReactNode;
};

const Layout = ({ children }: LayoutProps) => {
  return (
    <Router>
      <Header />
      <main>{children}</main>
      <Footer />
    </Router>
  );
};

export default Layout;
