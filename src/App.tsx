import React from "react";
import { Container } from "react-bootstrap";
import { Route, Switch } from "react-router-dom";

import Layout from "./layout/Layout";
import Home from "./pages/Home";
import Properties from "./pages/Properties";

function App() {
  return (
    <Layout>
      <Container>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/properties" component={Properties} />
        </Switch>
      </Container>
    </Layout>
  );
}

export default App;
