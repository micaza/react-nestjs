import { Col, Container, Row, Card, Button } from "react-bootstrap";
import Property from "../types/properties.type";

type PropertyListType = {
  properties: Property[];
};

function PropertyList({ properties }: PropertyListType) {
  return (
    <>
      <Container>
        <Row>
          {properties.map(({ id, name }) => (
            <Col key={`row-${id}`} lg={4} md={6} xs={12}>
              <Card className="mx-auto my-2">
                <Card.Img
                  variant="top"
                  src="https://images.unsplash.com/photo-1549517045-bc93de075e53?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZmFtaWx5JTIwaG91c2V8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                />
                <Card.Body>
                  <Card.Title>Card Title</Card.Title>
                  <Card.Text>
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </Card.Text>
                  <Button variant="primary">Go somewhere</Button>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}

export default PropertyList;
