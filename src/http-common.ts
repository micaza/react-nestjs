import axios, { AxiosRequestConfig, AxiosInstance } from "axios";

const options: AxiosRequestConfig = {
  baseURL: process.env.REACT_APP_API_BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
};

const httpClient: AxiosInstance = axios.create(options);

export default httpClient;
