import { useEffect, useState } from "react";

import Property from "../types/properties.type";
import PropertyList from "../components/PropertyList";
import PropertyService from "../services/property.service";

function Properties() {
  const [properties, setProperties] = useState<Property[]>([]);

  useEffect(() => {
    PropertyService.getProperties()
      .then(({ data }) => {
        setProperties(data);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);
  return <PropertyList properties={properties} />;
}

export default Properties;
