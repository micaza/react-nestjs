export default interface Property {
  id: number;
  category: string;
  image: string;
  name: string;
  price: number;
  skus: object[];
  description: string;
}
