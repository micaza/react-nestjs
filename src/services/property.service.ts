import { AxiosResponse, AxiosInstance } from "axios";
import httpClient from "../http-common";

class PropertyService {
  constructor(private http: AxiosInstance) {}

  getProperties(): Promise<AxiosResponse> {
    return this.http.get("/properties");
  }
}

export default new PropertyService(httpClient);
